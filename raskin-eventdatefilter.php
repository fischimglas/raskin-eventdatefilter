<?php
/**
 * Plugin Name: Raskin Event Date Filter
 * Description: Sets Event Filter to current Date for Homepage and Search Results, hides .block-type-countdown if date_from < today.
 * Version: 0.1
 * Last Update: 2019-05-04
 * Author: Jam
 * Author URI: https://hashcookie.ch
 **/

add_action('wp_enqueue_scripts', 'addEventFilterDefaultDateJs');

function addEventFilterDefaultDateJs()
{
	wp_enqueue_script('lodash', plugins_url('/lodash.min.js', __FILE__));
	wp_enqueue_script('moment', plugins_url('/moment.min.js', __FILE__));
	wp_enqueue_script('eventFilterDefaultDate', plugins_url('/eventFilterDefaultDate.js', __FILE__), array('jquery'));
}