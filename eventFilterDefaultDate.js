/**
 * Do the stuff.
 */
function setStartDate() {
  var today = moment().format('YYYY-MM-DD');
  // Add searchstring to Homepage, if not set.
  if(location.pathname === '/' && location.search === '') {
    location.search = '?type=event&date_from=' + today + '&sort=latest';
  }
  if(location.pathname === '/events/' && location.search === '') {
    location.search = '?type=event&date_from=' + today + '&sort=latest';
  }
  // Change Menu URL for events to directly open with search string
  jQuery('.menu-item').each(function() {
    var menuItem = jQuery(this);
    var a = menuItem.find('a');
    var url = a.attr('href');
    if(url.match('/events/')) {
      a.attr('href', url + '?type=event&date_from=' + today + '&sort=latest')
    }
  });
  /**
   * hide *element* if date_from < today
   */
  function hideOrShowElement() {
    var element = jQuery('.block-type-countdown');
    var res = /date_from=([^&]+)/i.exec(location.search);
    if(res && res[1]) {
      if(moment(res[1]).isBefore(today)) {
        element.hide();
      } else {
        element.show();
      }
    }
  }
  // Check for changes of Date and trigger hideOrShowElement
  var field = jQuery('input.display-value');
  var fieldValue = field.val();
  setInterval(function() {
    if(fieldValue !== field.val()) {
      fieldValue = field.val();
      hideOrShowElement();
    }
  }, 500);
  hideOrShowElement();
}

/**
 * Init Script
 */
jQuery(function() {
  setStartDate();
});